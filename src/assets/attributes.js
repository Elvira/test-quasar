export default [
  {
    name: 'name',
    label: 'Part number',
    type: 'string',
    set: 'core',
    primary: true
  },
  {
    name: 'label',
    label: 'Designation',
    type: 'localized string',
    set: 'core'
  },
  {
    name: 'price',
    label: 'Current price',
    type: 'currency',
    set: 'core'
  },
  {
    name: 'validated_price',
    label: 'Actual Price',
    set: 'pricing',
    type: 'currency',
    timeserie: true,
    weight: {
      type: 'currency'
    }
  },
  {
    name: 'turnover',
    label: 'Turnover',
    type: 'currency',
    set: 'sales',
    timeserie: true
  },
  {
    name: 'quantities',
    label: 'Sold units',
    type: 'number',
    set: 'sales',
    timeserie: true
  },
  {
    name: 'purchasing_cost',
    label: 'Purchasing cost',
    type: 'currency',
    set: 'sales',
    timeserie: true
  },
  {
    name: 'weight',
    label: 'Weight',
    type: 'number',
    set: 'dimensions'
  },
  {
    name: 'sap_code',
    label: 'SAP Code',
    type: 'string',
    set: 'other',
    groupable: true
  },
  {
    name: 'material_code',
    label: 'Material Code',
    type: 'string',
    set: 'other',
    groupable: true
  },
  {
    name: 'steel',
    label: 'Steel',
    set: 'materials',
    type: 'boolean',
    weight: {
      type: 'rate',
      value: 1
    }
  },
  {
    name: 'copper',
    label: 'Copper',
    set: 'materials',
    type: 'boolean',
    weight: {
      type: 'rate',
      value: 0.8
    }
  },
  {
    name: 'brackets',
    label: 'Mouting brackets',
    set: 'features',
    type: 'number',
    weight: {
      type: 'currency',
      value: {
        fixed: '30.00',
        float: 30,
        currency: 'USD'
      }
    }
  }
]
