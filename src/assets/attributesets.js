export default [
  {
    name: 'core',
    label: 'Core',
    fixed: true, // will be part of the fixed header
    show: 'all'
  },
  {
    name: 'pricing',
    label: 'Pricing',
    fixed: true,
    show: 'all'
  },
  {
    name: 'sales',
    label: 'Sales figures',
    show: 'all'
  },
  {
    name: 'dimensions',
    label: 'Dimensions',
    show: 'all+rule'
  },
  {
    name: 'other',
    label: 'Other data',
    show: 'all'
  },
  {
    name: 'materials',
    label: 'Materials',
    multiple: false,
    show: 'rule'
  },
  {
    name: 'features',
    label: 'Features',
    show: 'rule'
  }
]
