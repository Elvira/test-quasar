export default [
  {
    'photo': [
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92',
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92'
    ],
    'family': 'ad4b6a87-a7fb-4628-8413-d06afca8dc31',
    'attributes': [
      {
        'value': 'BSA00710000HO',
        'set': 'core',
        'name': 'name'
      },
      {
        'value': 'BARRE DE PINCES COMPLETE SP 126/1260',
        'name': 'label',
        'set': 'core'
      },
      {
        'value': 'BARRE DE PINCE',
        'name': 'xxxx',
        'set': ''
      },
      {
        'name': 'purchasing_cost',
        'set': 'sales',
        'value': {
          'number': 1566.95,
          'fixed': '1566.95',
          'currency': 'CHF'
        }
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3650.02,
          'fixed': '3650.02'
        },
        'set': 'core',
        'name': 'price'
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3946.95,
          'fixed': '3946.95'
        },
        'name': 'proposed_price',
        'set': 'pricing'
      },
      {
        'set': 'pricing',
        'name': 'raw_proposed_price',
        'value': {
          'fixed': '3946.95',
          'number': 3946.95,
          'currency': 'CHF'
        }
      },
      {
        'set': 'status',
        'name': 'part_status',
        'value': 'ko'
      },
      {
        'set': 'status',
        'name': 'price_status',
        'value': 'manual'
      },
      {
        'name': 'purchase_alert',
        'set': 'status',
        'value': false
      },
      {
        'value': '06.12.2017 09:55',
        'name': 'validation_date',
        'set': 'core'
      },
      {
        'set': 'dimensions',
        'name': 'weight',
        'value': {
          'number': 5500,
          'unit': 'g'
        }
      },
      {
        'value': {
          'unit': 'mm',
          'number': 28
        },
        'name': 'height',
        'set': 'dimensions'
      },
      {
        'name': 'length',
        'set': 'dimensions',
        'value': {
          'number': 1392,
          'unit': 'mm'
        }
      },
      {
        'value': {
          'number': 107,
          'unit': 'mm'
        },
        'name': 'width',
        'set': 'dimensions'
      },
      {
        'set': 'sales',
        'name': 'sold_units',
        'value': {
          'number': 10
        }
      },
      {
        'name': 'turnover',
        'set': 'sales',
        'value': {
          'number': 0,
          'fixed': '0',
          'currency': 'CHF'
        }
      },
      {
        'set': 'other',
        'name': 'reference_price',
        'value': {
          'fixed': '3770',
          'number': 3770,
          'currency': 'CHF'
        }
      },
      {
        'name': 'material_code',
        'set': 'other',
        'value': 'Aluminium'
      },
      {
        'set': 'other',
        'name': 'sap_price',
        'value': {
          'fixed': '3650.02',
          'number': 3650.02,
          'currency': 'CHF'
        }
      },
      {
        'value': 'SP',
        'name': 'other',
        'set': 'other'
      },
      {
        'value': true,
        'name': 'tong_opening',
        'set': 'features'
      }
    ],
    'id': '18222'
  },
  {
    'photo': [
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92',
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92'
    ],
    'family': 'ad4b6a87-a7fb-4628-8413-d06afca8dc31',
    'attributes': [
      {
        'value': 'BSA00710000HO',
        'set': 'core',
        'name': 'name'
      },
      {
        'value': 'BARRE DE PINCES COMPLETE SP 126/1260',
        'name': 'label',
        'set': 'core'
      },
      {
        'value': 'BARRE DE PINCE',
        'name': 'xxxx',
        'set': ''
      },
      {
        'name': 'purchasing_cost',
        'set': 'sales',
        'value': {
          'number': 1566.95,
          'fixed': '1566.95',
          'currency': 'CHF'
        }
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3650.02,
          'fixed': '3650.02'
        },
        'set': 'core',
        'name': 'price'
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3946.95,
          'fixed': '3946.95'
        },
        'name': 'proposed_price',
        'set': 'pricing'
      },
      {
        'set': 'pricing',
        'name': 'raw_proposed_price',
        'value': {
          'fixed': '3946.95',
          'number': 3946.95,
          'currency': 'CHF'
        }
      },
      {
        'set': 'status',
        'name': 'part_status',
        'value': 'ko'
      },
      {
        'set': 'status',
        'name': 'price_status',
        'value': 'manual'
      },
      {
        'name': 'purchase_alert',
        'set': 'status',
        'value': false
      },
      {
        'value': '06.12.2017 09:55',
        'name': 'validation_date',
        'set': 'core'
      },
      {
        'set': 'dimensions',
        'name': 'weight',
        'value': {
          'number': 5500,
          'unit': 'g'
        }
      },
      {
        'value': {
          'unit': 'mm',
          'number': 28
        },
        'name': 'height',
        'set': 'dimensions'
      },
      {
        'name': 'length',
        'set': 'dimensions',
        'value': {
          'number': 1392,
          'unit': 'mm'
        }
      },
      {
        'value': {
          'number': 107,
          'unit': 'mm'
        },
        'name': 'width',
        'set': 'dimensions'
      },
      {
        'set': 'sales',
        'name': 'sold_units',
        'value': {
          'number': 10
        }
      },
      {
        'name': 'turnover',
        'set': 'sales',
        'value': {
          'number': 0,
          'fixed': '0',
          'currency': 'CHF'
        }
      },
      {
        'set': 'other',
        'name': 'reference_price',
        'value': {
          'fixed': '3770',
          'number': 3770,
          'currency': 'CHF'
        }
      },
      {
        'name': 'material_code',
        'set': 'other',
        'value': 'Aluminium'
      },
      {
        'set': 'other',
        'name': 'sap_price',
        'value': {
          'fixed': '3650.02',
          'number': 3650.02,
          'currency': 'CHF'
        }
      },
      {
        'value': 'SP',
        'name': 'other',
        'set': 'other'
      },
      {
        'value': true,
        'name': 'tong_opening',
        'set': 'features'
      }
    ],
    'id': '18222'
  },
  {
    'photo': [
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92',
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92'
    ],
    'family': 'ad4b6a87-a7fb-4628-8413-d06afca8dc31',
    'attributes': [
      {
        'value': 'BSA00710000HO',
        'set': 'core',
        'name': 'name'
      },
      {
        'value': 'BARRE DE PINCES COMPLETE SP 126/1260',
        'name': 'label',
        'set': 'core'
      },
      {
        'value': 'BARRE DE PINCE',
        'name': 'xxxx',
        'set': ''
      },
      {
        'name': 'purchasing_cost',
        'set': 'sales',
        'value': {
          'number': 1566.95,
          'fixed': '1566.95',
          'currency': 'CHF'
        }
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3650.02,
          'fixed': '3650.02'
        },
        'set': 'core',
        'name': 'price'
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3946.95,
          'fixed': '3946.95'
        },
        'name': 'proposed_price',
        'set': 'pricing'
      },
      {
        'set': 'pricing',
        'name': 'raw_proposed_price',
        'value': {
          'fixed': '3946.95',
          'number': 3946.95,
          'currency': 'CHF'
        }
      },
      {
        'set': 'status',
        'name': 'part_status',
        'value': 'ko'
      },
      {
        'set': 'status',
        'name': 'price_status',
        'value': 'manual'
      },
      {
        'name': 'purchase_alert',
        'set': 'status',
        'value': false
      },
      {
        'value': '06.12.2017 09:55',
        'name': 'validation_date',
        'set': 'core'
      },
      {
        'set': 'dimensions',
        'name': 'weight',
        'value': {
          'number': 5500,
          'unit': 'g'
        }
      },
      {
        'value': {
          'unit': 'mm',
          'number': 28
        },
        'name': 'height',
        'set': 'dimensions'
      },
      {
        'name': 'length',
        'set': 'dimensions',
        'value': {
          'number': 1392,
          'unit': 'mm'
        }
      },
      {
        'value': {
          'number': 107,
          'unit': 'mm'
        },
        'name': 'width',
        'set': 'dimensions'
      },
      {
        'set': 'sales',
        'name': 'sold_units',
        'value': {
          'number': 10
        }
      },
      {
        'name': 'turnover',
        'set': 'sales',
        'value': {
          'number': 0,
          'fixed': '0',
          'currency': 'CHF'
        }
      },
      {
        'set': 'other',
        'name': 'reference_price',
        'value': {
          'fixed': '3770',
          'number': 3770,
          'currency': 'CHF'
        }
      },
      {
        'name': 'material_code',
        'set': 'other',
        'value': 'Aluminium'
      },
      {
        'set': 'other',
        'name': 'sap_price',
        'value': {
          'fixed': '3650.02',
          'number': 3650.02,
          'currency': 'CHF'
        }
      },
      {
        'value': 'SP',
        'name': 'other',
        'set': 'other'
      },
      {
        'value': true,
        'name': 'tong_opening',
        'set': 'features'
      }
    ],
    'id': '18222'
  },
  {
    'photo': [
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92',
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92'
    ],
    'family': 'ad4b6a87-a7fb-4628-8413-d06afca8dc31',
    'attributes': [
      {
        'value': 'BSA00710000HO',
        'set': 'core',
        'name': 'name'
      },
      {
        'value': 'BARRE DE PINCES COMPLETE SP 126/1260',
        'name': 'label',
        'set': 'core'
      },
      {
        'value': 'BARRE DE PINCE',
        'name': 'xxxx',
        'set': ''
      },
      {
        'name': 'purchasing_cost',
        'set': 'sales',
        'value': {
          'number': 1566.95,
          'fixed': '1566.95',
          'currency': 'CHF'
        }
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3650.02,
          'fixed': '3650.02'
        },
        'set': 'core',
        'name': 'price'
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3946.95,
          'fixed': '3946.95'
        },
        'name': 'proposed_price',
        'set': 'pricing'
      },
      {
        'set': 'pricing',
        'name': 'raw_proposed_price',
        'value': {
          'fixed': '3946.95',
          'number': 3946.95,
          'currency': 'CHF'
        }
      },
      {
        'set': 'status',
        'name': 'part_status',
        'value': 'ko'
      },
      {
        'set': 'status',
        'name': 'price_status',
        'value': 'manual'
      },
      {
        'name': 'purchase_alert',
        'set': 'status',
        'value': false
      },
      {
        'value': '06.12.2017 09:55',
        'name': 'validation_date',
        'set': 'core'
      },
      {
        'set': 'dimensions',
        'name': 'weight',
        'value': {
          'number': 5500,
          'unit': 'g'
        }
      },
      {
        'value': {
          'unit': 'mm',
          'number': 28
        },
        'name': 'height',
        'set': 'dimensions'
      },
      {
        'name': 'length',
        'set': 'dimensions',
        'value': {
          'number': 1392,
          'unit': 'mm'
        }
      },
      {
        'value': {
          'number': 107,
          'unit': 'mm'
        },
        'name': 'width',
        'set': 'dimensions'
      },
      {
        'set': 'sales',
        'name': 'sold_units',
        'value': {
          'number': 10
        }
      },
      {
        'name': 'turnover',
        'set': 'sales',
        'value': {
          'number': 0,
          'fixed': '0',
          'currency': 'CHF'
        }
      },
      {
        'set': 'other',
        'name': 'reference_price',
        'value': {
          'fixed': '3770',
          'number': 3770,
          'currency': 'CHF'
        }
      },
      {
        'name': 'material_code',
        'set': 'other',
        'value': 'Aluminium'
      },
      {
        'set': 'other',
        'name': 'sap_price',
        'value': {
          'fixed': '3650.02',
          'number': 3650.02,
          'currency': 'CHF'
        }
      },
      {
        'value': 'SP',
        'name': 'other',
        'set': 'other'
      },
      {
        'value': true,
        'name': 'tong_opening',
        'set': 'features'
      }
    ],
    'id': '18222'
  },
  {
    'photo': [
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92',
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92'
    ],
    'family': 'ad4b6a87-a7fb-4628-8413-d06afca8dc31',
    'attributes': [
      {
        'value': 'BSA00710000HO',
        'set': 'core',
        'name': 'name'
      },
      {
        'value': 'BARRE DE PINCES COMPLETE SP 126/1260',
        'name': 'label',
        'set': 'core'
      },
      {
        'value': 'BARRE DE PINCE',
        'name': 'xxxx',
        'set': ''
      },
      {
        'name': 'purchasing_cost',
        'set': 'sales',
        'value': {
          'number': 1566.95,
          'fixed': '1566.95',
          'currency': 'CHF'
        }
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3650.02,
          'fixed': '3650.02'
        },
        'set': 'core',
        'name': 'price'
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3946.95,
          'fixed': '3946.95'
        },
        'name': 'proposed_price',
        'set': 'pricing'
      },
      {
        'set': 'pricing',
        'name': 'raw_proposed_price',
        'value': {
          'fixed': '3946.95',
          'number': 3946.95,
          'currency': 'CHF'
        }
      },
      {
        'set': 'status',
        'name': 'part_status',
        'value': 'ko'
      },
      {
        'set': 'status',
        'name': 'price_status',
        'value': 'manual'
      },
      {
        'name': 'purchase_alert',
        'set': 'status',
        'value': false
      },
      {
        'value': '06.12.2017 09:55',
        'name': 'validation_date',
        'set': 'core'
      },
      {
        'set': 'dimensions',
        'name': 'weight',
        'value': {
          'number': 5500,
          'unit': 'g'
        }
      },
      {
        'value': {
          'unit': 'mm',
          'number': 28
        },
        'name': 'height',
        'set': 'dimensions'
      },
      {
        'name': 'length',
        'set': 'dimensions',
        'value': {
          'number': 1392,
          'unit': 'mm'
        }
      },
      {
        'value': {
          'number': 107,
          'unit': 'mm'
        },
        'name': 'width',
        'set': 'dimensions'
      },
      {
        'set': 'sales',
        'name': 'sold_units',
        'value': {
          'number': 10
        }
      },
      {
        'name': 'turnover',
        'set': 'sales',
        'value': {
          'number': 0,
          'fixed': '0',
          'currency': 'CHF'
        }
      },
      {
        'set': 'other',
        'name': 'reference_price',
        'value': {
          'fixed': '3770',
          'number': 3770,
          'currency': 'CHF'
        }
      },
      {
        'name': 'material_code',
        'set': 'other',
        'value': 'Aluminium'
      },
      {
        'set': 'other',
        'name': 'sap_price',
        'value': {
          'fixed': '3650.02',
          'number': 3650.02,
          'currency': 'CHF'
        }
      },
      {
        'value': 'SP',
        'name': 'other',
        'set': 'other'
      },
      {
        'value': true,
        'name': 'tong_opening',
        'set': 'features'
      }
    ],
    'id': '18222'
  },
  {
    'photo': [
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92',
      'assets/0d033f59-829e-48e8-9aa3-ad1cc8693f92'
    ],
    'family': 'ad4b6a87-a7fb-4628-8413-d06afca8dc31',
    'attributes': [
      {
        'value': 'BSA00710000HO',
        'set': 'core',
        'name': 'name'
      },
      {
        'value': 'BARRE DE PINCES COMPLETE SP 126/1260',
        'name': 'label',
        'set': 'core'
      },
      {
        'value': 'BARRE DE PINCE',
        'name': 'xxxx',
        'set': ''
      },
      {
        'name': 'purchasing_cost',
        'set': 'sales',
        'value': {
          'number': 1566.95,
          'fixed': '1566.95',
          'currency': 'CHF'
        }
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3650.02,
          'fixed': '3650.02'
        },
        'set': 'core',
        'name': 'price'
      },
      {
        'value': {
          'currency': 'CHF',
          'number': 3946.95,
          'fixed': '3946.95'
        },
        'name': 'proposed_price',
        'set': 'pricing'
      },
      {
        'set': 'pricing',
        'name': 'raw_proposed_price',
        'value': {
          'fixed': '3946.95',
          'number': 3946.95,
          'currency': 'CHF'
        }
      },
      {
        'set': 'status',
        'name': 'part_status',
        'value': 'ko'
      },
      {
        'set': 'status',
        'name': 'price_status',
        'value': 'manual'
      },
      {
        'name': 'purchase_alert',
        'set': 'status',
        'value': false
      },
      {
        'value': '06.12.2017 09:55',
        'name': 'validation_date',
        'set': 'core'
      },
      {
        'set': 'dimensions',
        'name': 'weight',
        'value': {
          'number': 5500,
          'unit': 'g'
        }
      },
      {
        'value': {
          'unit': 'mm',
          'number': 28
        },
        'name': 'height',
        'set': 'dimensions'
      },
      {
        'name': 'length',
        'set': 'dimensions',
        'value': {
          'number': 1392,
          'unit': 'mm'
        }
      },
      {
        'value': {
          'number': 107,
          'unit': 'mm'
        },
        'name': 'width',
        'set': 'dimensions'
      },
      {
        'set': 'sales',
        'name': 'sold_units',
        'value': {
          'number': 10
        }
      },
      {
        'name': 'turnover',
        'set': 'sales',
        'value': {
          'number': 0,
          'fixed': '0',
          'currency': 'CHF'
        }
      },
      {
        'set': 'other',
        'name': 'reference_price',
        'value': {
          'fixed': '3770',
          'number': 3770,
          'currency': 'CHF'
        }
      },
      {
        'name': 'material_code',
        'set': 'other',
        'value': 'Aluminium'
      },
      {
        'set': 'other',
        'name': 'sap_price',
        'value': {
          'fixed': '3650.02',
          'number': 3650.02,
          'currency': 'CHF'
        }
      },
      {
        'value': 'SP',
        'name': 'other',
        'set': 'other'
      },
      {
        'value': true,
        'name': 'tong_opening',
        'set': 'features'
      }
    ],
    'id': '18222'
  }
]
