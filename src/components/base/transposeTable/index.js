import QTTable from './QTTable.js'
import QTh from './QTh.js'
import QTr from './QTr.js'
import QTd from './QTd.js'
import QTableColumns from './QTableColumns.js'

export {
  QTTable,
  QTh,
  QTr,
  QTd,
  QTableColumns
}
