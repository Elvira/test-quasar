// import QIcon from '../../../../node_modules/quasar-framework/src/components/icon/QIcon.js'

export default {
  name: 'HTh',
  props: {
    props: Object,
    autoWidth: Boolean
  },
  render (h) {
    let col = this.props.col
    console.log('HTh')
    return h('th', {
      'class': [{
        'h-table-col-auto-width': this.autoWidth
      }]
    },
    [
      h('img', {class: col.photoClass, attrs: {src: col.photo}})
    ])
  }
}
