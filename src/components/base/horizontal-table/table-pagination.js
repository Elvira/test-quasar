function samePagination (oldPag, newPag) {
  for (let prop in newPag) {
    if (newPag[prop] !== oldPag[prop]) {
      return false
    }
  }
  return true
}

function fixPagination (p) {
  if (p.page < 1) {
    p.page = 1
  }
  if (p.partsPerPage !== void 0 && p.partsPerPage < 1) {
    p.partsPerPage = 0
  }
  return p
}

export default {
  props: {
    pagination: Object,
    partsPerPageOptions: {
      type: Array,
      default: () => [3, 5, 7, 10, 15, 20, 25, 50, 0]
    }
  },
  data () {
    return {
      innerPagination: {
        sortBy: null,
        descending: false,
        page: 1,
        partsPerPage: 5
      }
    }
  },
  computed: {
    computedPagination () {
      return fixPagination(Object.assign({}, this.innerPagination, this.pagination))
    },
    firstPartIndex () {
      const { page, partsPerPage } = this.computedPagination
      return (page - 1) * partsPerPage
    },
    lastPartIndex () {
      const { page, partsPerPage } = this.computedPagination
      return page * partsPerPage
    },
    isFirstPage () {
      return this.computedPagination.page === 1
    },
    pagesNumber () {
      return Math.max(
        1,
        Math.ceil(this.computedPartsNumber / this.computedPagination.partsPerPage)
      )
    },
    isLastPage () {
      if (this.lastPartIndex === 0) {
        return true
      }
      return this.computedPagination.page >= this.pagesNumber
    },
    computedPartsPerPageOptions () {
      return this.partsPerPageOptions.map(count => ({
        label: count === 0 ? this.$q.i18n.table.allRows : '' + count,
        value: count
      }))
    }
  },
  watch: {
    pagesNumber (lastPage, oldLastPage) {
      if (lastPage === oldLastPage) {
        return
      }

      const currentPage = this.computedPagination.page
      if (lastPage && !currentPage) {
        this.setPagination({ page: 1 })
      } else if (lastPage < currentPage) {
        this.setPagination({ page: lastPage })
      }
    }
  },
  methods: {
    setPagination (val, forceServerRequest) {
      const newPagination = fixPagination(Object.assign({}, this.computedPagination, val))

      if (samePagination(this.computedPagination, newPagination)) {
        return
      }

      if (this.pagination) {
        this.$emit('update:pagination', newPagination)
      } else {
        this.innerPagination = newPagination
      }
    },
    prevPage () {
      const { page } = this.computedPagination
      if (page > 1) {
        this.setPagination({page: page - 1})
      }
    },
    nextPage () {
      const { page, partsPerPage } = this.computedPagination
      if (this.lastPartIndex > 0 && page * partsPerPage < this.computedPartsNumber) {
        this.setPagination({page: page + 1})
      }
    }
  },
  created () {
    this.$emit('update:pagination', Object.assign({}, this.computedPagination))
  }
}
