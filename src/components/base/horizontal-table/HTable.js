import TableHeader from './table-header.js'
import TableBody from './table-body.js'
// import Bottom from './table-bottom.js'

// import Sort from './table-sort.js'
// import Filter from './table-filter.js'
// import Pagination from './table-pagination.js'
// import RowSelection from './table-row-selection.js'
// import ColumnSelection from './table-column-selection.js'
// import Expand from './table-expand.js'

// function pluck(array, key) {
//   return array.map(o => o[key]);
// }

export default {
  name: 'HTable',
  mixins: [
    TableHeader,
    TableBody
    // Bottom,
    // Sort,
    // Filter,
    // Pagination,
    // RowSelection,
    // ColumnSelection,
    // Expand
  ],
  props: {
    data: {
      type: Array,
      default: () => []
    },
    rowKey: {
      type: String,
      default: 'name'
    },
    colKey: {
      type: String,
      default: 'id'
    },
    attributes: Array,
    attributesets: Array
  },
  computed: {
    computedData () {
      // const { sortBy, descending, partsPerPage } = this.computedPagination
      let parts = this.data
      // if (this.columnToSort) {
      //   parts = this.sortMethod(parts, sortBy, descending)
      // }
      // if (this.filter) {
      //   parts = this.filterMethod(parts, this.filter, this.computedParts, this.getCellValue)
      // }

      // if (partsPerPage) {
      //   parts.slice(this.firstPartIndex, this.lastPartIndex)
      // }
      const partsNumber = parts.length
      return {
        partsNumber, parts
      }
    },
    nothingToDisplay () {
      return this.computedData.length === 0
    }
  },
  render (h) {
    return h('div',
      {
        'class': [
          'h-table-dense',
          {
            'h-table-container': true
          }
        ]
      },
      [
        this.getBody(h)
        // this.getBottom(h)
      ]
    )
  },
  methods: {
    getBody (h) {
      return h('div', { staticClass: 'h-table-middle scroll' }, [
        h('table', { staticClass: 'h-table' },
          [
            this.getTableHeader(h),
            this.getTableBody(h)
          ]
        )
      ])
    }
  }
}
