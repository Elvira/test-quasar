// import QProgress from '../../../../node_modules/quasar-framework/src/components/progress/QProgress.js'
// import QCheckbox from '../../../../node_modules/quasar-framework/src/components/checkbox/QCheckbox.js'
import HTh from './HTh.js'

export default {
  methods: {
    getTableHeader (h) {
      const child = [ this.getTableHeaderRow(h) ]
      return h('thead', {class: 'bg-grey-4'}, child)
    },
    getTableHeaderRow (h) {
      const child = this.computedData.parts.map((col) => {
        return h(HTh, {
          props: {
            props: {
              col: {
                photoClass: 'h-table-image',
                photo: (col.photo.length > 0) ? col.photo[0] : null
              }
            }
          }
        })
      })
      child.unshift(h('th', {class: 'h-table-col-attr'}, [' ']))
      return h('tr', child)
    }
  }
}
