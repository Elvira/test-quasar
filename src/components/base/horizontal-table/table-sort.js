//import { sortDate } from '../../../../node_modules/quasar-framework/src/utils/sort.js'
//import { isNumber } from '../../../../node_modules/quasar-framework/src/utils/is.js'

export default {
  props: {
    sortMethod: {
      type: Function,
      default (data, sortBy, descending) {
        const attr = this.attributes.find(def => def.name === sortBy)
        if (attr === null) {
          return data
        }

        const
          dir = descending ? -1 : 1,
          val = v => v[attr.name]

        return data.sort((a, b) => {
          let
            A = val(a),
            B = val(b)

          if (A === null || A === void 0) {
            return -1 * dir
          }
          if (B === null || B === void 0) {
            return 1 * dir
          }
          if (attribute.sort) {
            return col.sort(A, B) * dir
          }
          if (attribute.type === 'number') {
            return (A - B) * dir
          }
          if (attribute.type === 'currency') {
            return (A - B) * dir
          }
          if (attribute.type === 'boolean') {
            return (a - b) * dir
          }

          [A, B] = [A, B].map(s => (s + '').toLowerCase())

          return A < B
            ? -1 * dir
            : (A === B ? 0 : dir)
        })
      }
    }
  },
  computed: {
    attrToSort () {
      const { sortBy } = this.computedPagination

      if (sortBy) {
        return this.attributes.find(def => def.name === sortBy) || null
      }
    }
  },
  methods: {
    sort (attr /* String(col name) or Object(col definition) */) {
      if (attr === Object(attr)) {
        attr = attr.name
      }

      let { sortBy, descending } = this.computedPagination

      if (sortBy !== attr) {
        sortBy = attr
        descending = false
      } else {
        if (this.binaryStateSort) {
          descending = !descending
        } else {
          if (descending) {
            sortBy = null
          } else {
            descending = true
          }
        }
      }

      this.setPagination({ sortBy, descending, page: 1 })
    }
  }
}
