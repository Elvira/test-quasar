// import QCheckbox from '../../../../node_modules/quasar-framework/src/components/checkbox/QCheckbox.js'
// import QIcon from '../../../../node_modules/quasar-framework/src/components/icon/QIcon.js'

export default {
  methods: {
    getTableBody (h) {
      let child = this.attributesets.map(rowAttrSet => {
        let child = [h('tr', {class: ['h-table-attr-group', {'bg-grey-4': rowAttrSet.fixed}]}, [h('td', {attrs: {colspan: (this.attributes.length + 1)}}, [h('span', {class: ['h-table-chip', 'bg-grey-8']}, rowAttrSet.label)])])]
        let rows = this.attributes.reduce((memo, rowAttr) => {
          if (rowAttr.set === rowAttrSet.name) {
            let row = this.computedData.parts.reduce((memo, part) => {
              let attribute = part.attributes.find(attr => {
                return attr.name === rowAttr.name
              })
              let value = ''
              if (attribute) {
                if (rowAttr.type === 'string' || rowAttr.type === 'localized string') {
                  value = attribute.value
                }
                if (rowAttr.type === 'number') {
                  value = attribute.value.number + ' ' + attribute.value.unit
                }
                if (rowAttr.type === 'currency') {
                  value = attribute.value.fixed + ' ' + attribute.value.currency
                }
                if (rowAttr.type === 'boolean' && attribute.value) {
                  value = 'X'
                }
              }
              memo.push(h('td', {class: 'h-table-cell'}, value))
              return memo
            }, [h('td', {class: 'h-table-attr-name'}, rowAttr.label)])
            memo.push(h('tr', {class: ['h-table-attr', {'bg-grey-4': rowAttrSet.fixed}]}, row))
          }
          return memo
        }, [])
        child.push(rows)
        return child
      })
      return h('tbody', child)
    }
  }
}
